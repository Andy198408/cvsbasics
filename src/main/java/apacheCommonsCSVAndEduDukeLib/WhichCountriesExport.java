package apacheCommonsCSVAndEduDukeLib;

import edu.duke.FileResource;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;


public class WhichCountriesExport {
    public void listExporters(CSVParser parser, String exportOfInterest) {
        for (CSVRecord record : parser) {
            String export = record.get("Exports");
            if (export.contains(exportOfInterest)) {
                String country = record.get("Country");
                System.out.println(country);
            }
        }
    }

    public String countryInfo(CSVParser parser, String country) {
        StringBuilder sb = new StringBuilder();
        for (CSVRecord record : parser) {
            String recCountry = record.get("Country");
            if (recCountry.contains(country)) {
                sb.append(record.get("Country"));
                sb.append(" : ");
                sb.append(record.get("Exports"));
                sb.append(" : ");
                sb.append(record.get("Value (dollars)"));
            }
            if (sb.toString().isEmpty()) {
                sb.append("Not found!");
            }
        }
        return sb.toString();
    }

    public void listExportersTwoProducts(String export1, String export2) {
        CSVParser parser = getCsvParser();
        for (CSVRecord record : parser) {
            String export = record.get("Exports");
            if (export.contains(export1) && export.contains(export2)) {
                String country = record.get("Country");
                System.out.println(country);
            }
        }
    }

    public void whoExportsCoffee() {
        CSVParser parser = getCsvParser();
        listExporters(parser, "coffee");
    }

    public String infoOfCountry(String country) {
        CSVParser parser = getCsvParser();
        return countryInfo(parser, country);
    }

    private CSVParser getCsvParser() {
        FileResource fr = new FileResource();
        return fr.getCSVParser();
    }

}
