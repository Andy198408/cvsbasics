package javaCore;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CsvReader {
    static String COMMA_DELIMETER = ",";

    public static void main(String[] args) {
        List<List<String>> records = new ArrayList<>();

        try (
                BufferedReader br = new BufferedReader(new FileReader("D:\\obelisk\\workingWithCSV\\src\\main\\resources\\weather-2012-01-01.csv"))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(COMMA_DELIMETER);
                records.add(Arrays.asList(values));
            }
        } catch (IOException ioe) {
            System.err.println(ioe);
        }

        System.out.println(records.size());
        records.forEach(System.out::println);
    }
}

