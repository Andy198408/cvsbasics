package apacheCommonsCSVLib.exercise;

import Utility.MyFileResource;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import java.io.*;
import java.util.*;

public class ReadingStoringWritingCSV {
    enum MyHeaders {
        Country, Exports, Value
    }

    public static void main(String[] args) throws IOException {
        TreeMap<String, ArrayList<String>> countryMap = new TreeMap<>();
        MyFileResource fr = new MyFileResource();
        File f = fr.openFile();

        if (f != null) {
            //        Reader in = new FileReader("src/main/resources/exports/exports_small.csv");
            Reader in = new FileReader(f);

            Iterable<CSVRecord> records = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(in);
            for (CSVRecord record : records) {
                String country = record.get("Country");
                String exports = record.get("Exports");
                String value = record.get("Value");

                countryMap.put(country, new ArrayList<>(Arrays.asList(exports, value)));

                System.out.printf("\nCountry : %s  Exports : %s  Value : %s", country, exports, value);
            }
            System.out.println("\n");
            System.out.println(countryMap.keySet());

            String path = fr.saveFile();
//            FileWriter out = new FileWriter("src/main/resources/MyExport.csv");
            FileWriter out = new FileWriter(path);

            try (CSVPrinter printer = CSVFormat.DEFAULT.withHeader(MyHeaders.class).print(out)) {
                countryMap.forEach((k, list) -> {
                    try {
                        list.add(0, k);
                        printer.printRecord(list);
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                });
            }

            System.out.println("\nReproduction of given list!");
            in = new FileReader(path);
            Iterable<CSVRecord> recordsAfterWrite = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(in);
            for (CSVRecord record : recordsAfterWrite) {
                String country = record.get("Country");
                String exports = record.get("Exports");
                String value = record.get("Value");

                System.out.printf("\nCountry : %s  Exports : %s  Value : %s", country, exports, value);
            }
        } else {
            System.out.println("No file was chosen");
        }
    }
}
