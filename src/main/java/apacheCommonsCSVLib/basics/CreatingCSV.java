package apacheCommonsCSVLib.basics;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CreatingCSV {
    public static void main(String[] args) throws IOException {
        createCSVFile();
    }

    public static void createCSVFile() throws IOException {
        Map<String, String> AUTHOR_BOOK_MAP = new HashMap<>() {
            {
                put("Dan Simmons", "Hyperion");
                put("Douglas Adams", "The Hitchhiker's Guide to the Galaxy");
            }
        };
        String[] HEADERS = {"author", "title"};

        FileWriter out = new FileWriter("src/main/resources/book_new.csv");

        try (CSVPrinter printer =
                     new CSVPrinter(out, CSVFormat.DEFAULT.withHeader(HEADERS))) {
            AUTHOR_BOOK_MAP.forEach(
                    (author, title) -> {
                        try {
                            printer.printRecord(author, title);
                        } catch (IOException ioException) {
                            ioException.printStackTrace();
                        }
                    }
            );
        } catch (IOException ioException) {
            System.out.println(ioException);
        }
    }
}
