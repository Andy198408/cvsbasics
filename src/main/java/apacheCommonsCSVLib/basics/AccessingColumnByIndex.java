package apacheCommonsCSVLib.basics;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class AccessingColumnByIndex {
    public static void main(String[] args) throws IOException {
        Reader in = new FileReader("src/main/resources/book.csv");
//        skips first record
//        Iterable<CSVRecord> records = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(in);
        Iterable<CSVRecord> records = CSVFormat.DEFAULT.parse(in);
        for (CSVRecord record : records) {
            String columnOne = record.get(0);
            String columnTwo = record.get(1);
            System.out.printf("\nColumn one: %s and column two: %s", columnOne, columnTwo);
        }
    }
}
