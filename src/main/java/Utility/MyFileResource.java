package Utility;

import FileChooser.OracleFileChooserDemoProject.src.components.FileChooserDemo;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;

public class MyFileResource extends JPanel {
    JFileChooser fc = new JFileChooser("src/main/resources");
    FileNameExtensionFilter filter = new FileNameExtensionFilter(
            "CSV files", "csv");


    public File openFile() {
        File file = null;
        fc.setDialogTitle("Open a file");
        int returnVal = fc.showOpenDialog(MyFileResource.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            fc.setFileFilter(filter);
            file = fc.getSelectedFile();
        }
        return file;
    }

    public String saveFile() {
        fc.setDialogTitle("Choose save location");
        File file = null;
        int returnVal = fc.showSaveDialog(MyFileResource.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {

            file = fc.getSelectedFile();
        }
        return file.getAbsolutePath();
    }


}
